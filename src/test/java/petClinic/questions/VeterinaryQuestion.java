package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import petClinic.pageObjects.VeterinariansPageObject;

import java.util.List;

public class VeterinaryQuestion implements Question<List<String>> {

  String speciality;

  public VeterinaryQuestion(String speciality) {
    this.speciality = speciality;
  }

  public static Question<List<String>> getVetsBySpeciality(String speciality) {
    return new VeterinaryQuestion(speciality);
  }

  @Override
  public List<String> answeredBy(Actor actor) {
    return Text.of(VeterinariansPageObject.GET_VETS_BY_SPECIALITY.of(speciality))
            .viewedBy(actor)
            .asList();
  }
}
