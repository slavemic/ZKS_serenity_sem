package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.OWNER_INPUT_HELPER;

public class OwnerInputHelperQuestion implements Question<Integer> {

  public static Question<Integer> helperText() {
    return new OwnerInputHelperQuestion();
  }

  @Override
  public Integer answeredBy(Actor actor) {
    return Text.of(OWNER_INPUT_HELPER).viewedBy(actor).asString().length();
  }
}
