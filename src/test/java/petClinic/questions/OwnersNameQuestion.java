package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.OWNER_DETAIL_NAME;

public class OwnersNameQuestion implements Question<String> {

  public static Question<String> firstName() {
    return new OwnersNameQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    String fullName = Text.of(OWNER_DETAIL_NAME).viewedBy(actor).asString().trim();
    return fullName.substring(0, fullName.indexOf(" "));
  }
}
