package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_PET_VISIT_DATE;

public class PetVisitDateQuestion implements Question<String> {

  public static Question<String> date() {
    return new PetVisitDateQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(GET_PET_VISIT_DATE).viewedBy(actor).asString().trim();
  }
}
