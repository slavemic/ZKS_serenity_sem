package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class CurrentUserUrlQuestion implements Question<String> {
  String isOn;

  public CurrentUserUrlQuestion(String isOn) {
    this.isOn = isOn;
  }

  public static Question<String> currentUrl(String isOn) {
    return new CurrentUserUrlQuestion(isOn);
  }

  @Override
  public String answeredBy(Actor actor) {
    return isOn;
  }
}
