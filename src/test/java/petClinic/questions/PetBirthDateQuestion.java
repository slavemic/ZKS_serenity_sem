package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_PET_BIRTH;

public class PetBirthDateQuestion implements Question<String> {

  public static Question<String> date() {
    return new PetBirthDateQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(GET_PET_BIRTH).viewedBy(actor).asString().trim();
  }
}
