package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.VeterinariansPageObject.VETS_XML_OR_JSON;

public class VeterinaryJSONQuestion implements Question<Boolean> {


	public static Question<Boolean> doesContainJSON() {
		return new VeterinaryJSONQuestion();
	}

	@Override
	public Boolean answeredBy(Actor actor) {
    String vetsJSON =
        "{\"vetList\":[{\"id\":1,\"firstName\":\"James\",\"lastName\":\"Carter\",\"specialties\":[],\"nrOfSpecialties\":0,\"new\":false},{\"id\":3,\"firstName\":\"Linda\",\"lastName\":\"Douglas\",\"specialties\":[{\"id\":3,\"name\":\"dentistry\",\"new\":false},{\"id\":2,\"name\":\"surgery\",\"new\":false}],\"nrOfSpecialties\":2,\"new\":false},{\"id\":6,\"firstName\":\"Sharon\",\"lastName\":\"Jenkins\",\"specialties\":[],\"nrOfSpecialties\":0,\"new\":false},{\"id\":2,\"firstName\":\"Helen\",\"lastName\":\"Leary\",\"specialties\":[{\"id\":1,\"name\":\"radiology\",\"new\":false}],\"nrOfSpecialties\":1,\"new\":false},{\"id\":4,\"firstName\":\"Rafael\",\"lastName\":\"Ortega\",\"specialties\":[{\"id\":2,\"name\":\"surgery\",\"new\":false}],\"nrOfSpecialties\":1,\"new\":false},{\"id\":5,\"firstName\":\"Henry\",\"lastName\":\"Stevens\",\"specialties\":[{\"id\":1,\"name\":\"radiology\",\"new\":false}],\"nrOfSpecialties\":1,\"new\":false}]}";


		return Text.of(VETS_XML_OR_JSON).viewedBy(actor).asString().equals(vetsJSON);
	}
}
