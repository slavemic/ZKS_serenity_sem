package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import petClinic.pageObjects.VeterinariansPageObject;

import java.util.List;

import static petClinic.pageObjects.VeterinariansPageObject.VETS_XML_OR_JSON;

public class VeterinaryXMLQuestion implements Question<Boolean> {


	public static Question<Boolean> doesContainXML() {
		return new VeterinaryXMLQuestion();
	}

	@Override
	public Boolean answeredBy(Actor actor) {
    String vetsXML =
        "This XML file does not appear to have any style information associated with it. The document tree is shown below.\n"
                + "<vets>\n"
                + "<vet>\n"
                + "<id>1</id>\n"
                + "<firstName>James</firstName>\n"
                + "<lastName>Carter</lastName>\n"
                + "</vet>\n"
                + "<vet>\n"
                + "<id>3</id>\n"
                + "<firstName>Linda</firstName>\n"
                + "<lastName>Douglas</lastName>\n"
                + "<specialties>\n"
                + "<id>3</id>\n"
                + "<name>dentistry</name>\n"
                + "</specialties>\n"
                + "<specialties>\n"
                + "<id>2</id>\n"
                + "<name>surgery</name>\n"
                + "</specialties>\n"
                + "</vet>\n"
                + "<vet>\n"
                + "<id>6</id>\n"
                + "<firstName>Sharon</firstName>\n"
                + "<lastName>Jenkins</lastName>\n"
                + "</vet>\n"
                + "<vet>\n"
                + "<id>2</id>\n"
                + "<firstName>Helen</firstName>\n"
                + "<lastName>Leary</lastName>\n"
                + "<specialties>\n"
                + "<id>1</id>\n"
                + "<name>radiology</name>\n"
                + "</specialties>\n"
                + "</vet>\n"
                + "<vet>\n"
                + "<id>4</id>\n"
                + "<firstName>Rafael</firstName>\n"
                + "<lastName>Ortega</lastName>\n"
                + "<specialties>\n"
                + "<id>2</id>\n"
                + "<name>surgery</name>\n"
                + "</specialties>\n"
                + "</vet>\n"
                + "<vet>\n"
                + "<id>5</id>\n"
                + "<firstName>Henry</firstName>\n"
                + "<lastName>Stevens</lastName>\n"
                + "<specialties>\n"
                + "<id>1</id>\n"
                + "<name>radiology</name>\n"
                + "</specialties>\n"
                + "</vet>\n"
                + "</vets>";

		return Text.of(VETS_XML_OR_JSON).viewedBy(actor).asString().equals(vetsXML);
	}
}
