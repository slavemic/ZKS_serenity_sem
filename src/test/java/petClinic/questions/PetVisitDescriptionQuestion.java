package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_PET_VISIT_DESCRIPTION;

public class PetVisitDescriptionQuestion implements Question<String> {

  public static Question<String> text() {
    return new PetVisitDescriptionQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(GET_PET_VISIT_DESCRIPTION).viewedBy(actor).asString().trim();
  }
}
