package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_PET_NAME;

public class PetNameQuestion implements Question<String> {

  public static Question<String> name() {
    return new PetNameQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(GET_PET_NAME).viewedBy(actor).asString().trim();
  }
}
