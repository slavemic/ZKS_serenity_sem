package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.SOMETHING_HAPPENED;

public class CreationErrorQuestion implements Question<String> {

  public static Question<String> headerText() {
    return new CreationErrorQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(SOMETHING_HAPPENED).viewedBy(actor).asString().trim();
  }
}
