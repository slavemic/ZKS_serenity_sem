package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_OWNERS_TABLE_ROWS;

public class FoundOwnersQuestion implements Question<Integer> {

  public static Question<Integer> theDisplayedOwnersCount() {
    return new FoundOwnersQuestion();
  }

  @Override
  public Integer answeredBy(Actor actor) {
    return Text.of(GET_OWNERS_TABLE_ROWS).viewedBy(actor).asList().size();
  }
}
