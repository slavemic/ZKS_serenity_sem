package petClinic.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static petClinic.pageObjects.FindOwnersPageObject.GET_PET_TYPE;

public class PetTypeQuestion implements Question<String> {

  public static Question<String> type() {
    return new PetTypeQuestion();
  }

  @Override
  public String answeredBy(Actor actor) {
    return Text.of(GET_PET_TYPE).viewedBy(actor).asString().trim();
  }
}
