package petClinic.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://slavev.stepanek.app/owners/find")
public class FindOwnersPageObject extends PageObject {

  public static final Target FIND_OWNER_LASTNAME_INPUT =
      Target.the("Owners last name input")
          .locatedBy("/html/body/div/div/form/div[1]/div/div/input");

  public static final Target FIND_OWNER_BUTTON =
      Target.the("Find owner button").locatedBy("/html/body/div/div/form/div[2]/div/button");

  public static final Target ADD_OWNER_BUTTON =
      Target.the("Add owner button").locatedBy("/html/body/div/div/a");

  // FOUND OWNERS
  public static final Target GET_OWNERS_TABLE_ROWS =
      Target.the("owners table").locatedBy("/html/body/div/div/table/tbody/tr");

  // ADD/UPDATE OWNER
  public static final Target OWNER_FIRSTNAME_INPUT =
      Target.the("New owner name input")
          .locatedBy("/html/body/div/div/form/div[1]/div[1]/div/input");

  public static final Target OWNER_LASTNAME_INPUT =
      Target.the("New owner lastname input")
          .locatedBy("/html/body/div/div/form/div[1]/div[2]/div/input");

  public static final Target OWNER_ADDRESS_INPUT =
      Target.the("New owner address input")
          .locatedBy("/html/body/div/div/form/div[1]/div[3]/div/input");

  public static final Target OWNER_CITY_INPUT =
      Target.the("New owner city input")
          .locatedBy("/html/body/div/div/form/div[1]/div[4]/div/input");

  public static final Target OWNER_TELEPHONE_INPUT =
      Target.the("New owner telephone input")
          .locatedBy("/html/body/div/div/form/div[1]/div[5]/div/input");

  public static final Target OWNER_INPUT_HELPER =
      Target.the("Input helper for owner")
          .locatedBy("/html/body/div/div/form/div[1]/div/div/span[2]");

  public static final Target SOMETHING_HAPPENED =
      Target.the("Something happened error").locatedBy("/html/body/div/div/h2");

  public static final Target OWNER_ADD_BUTTON =
      Target.the("New owner add button").locatedBy("/html/body/div/div/form/div[2]/div/button");

  // OWNER DETAIL
  public static final Target EDIT_OWNER_BUTTON =
      Target.the("Edit owner button").locatedBy("/html/body/div/div/a[1]");

  public static final Target OWNER_DETAIL_NAME =
      Target.the("Edit owner button")
          .locatedBy("/html/body/div/div/table[1]/tbody/tr[1]/td/strong");

  public static final Target ADD_NEW_PET_BUTTON =
      Target.the("Add new pet button").locatedBy("/html/body/div/div/a[2]");

  public static final Target GET_PETS_AND_VISITS_TABLE =
      Target.the("Get pets and visits table").locatedBy("/html/body/div/div/table[2]");

  public static final Target GET_PET_EDIT =
      Target.the("Get pet edit button")
          .locatedBy("/html/body/div/div/table[2]/tbody/tr/td/table/tbody/tr/td[1]/a");

  public static final Target GET_PET_VISIT =
      Target.the("Get pet visit button")
          .locatedBy("/html/body/div/div/table[2]/tbody/tr/td/table/tbody/tr/td[2]/a");

  public static final Target GET_PET_NAME =
      Target.the("Get pets name").locatedBy("/html/body/div/div/table[2]/tbody/tr/th/dl/dd[1]");

  public static final Target GET_PET_BIRTH =
      Target.the("Get pets birth").locatedBy("/html/body/div/div/table[2]/tbody/tr/th/dl/dd[2]");

  public static final Target GET_PET_TYPE =
      Target.the("Get pets type").locatedBy("/html/body/div/div/table[2]/tbody/tr/th/dl/dd[3]");

  public static final Target GET_PET_VISIT_DATE =
      Target.the("Get pet visit date")
          .locatedBy("/html/body/div/div/table[2]/tbody/tr/td/table/tbody/tr[1]/td[1]");

  public static final Target GET_PET_VISIT_DESCRIPTION =
      Target.the("Get pet visit date")
          .locatedBy("/html/body/div/div/table[2]/tbody/tr/td/table/tbody/tr[1]/td[2]");

  // ADD/UPDATE NEW PET
  public static final Target PET_NAME_INPUT =
      Target.the("Pet name input").locatedBy("/html/body/div[1]/div/form/div[1]/div[2]/div/input");

  public static final Target PET_BIRT_DATE_INPUT =
      Target.the("Pet birth date").locatedBy("/html/body/div[1]/div/form/div[1]/div[3]/div/input");

  public static final Target PET_TYPE_SELECT_INPUT =
      Target.the("Pet type").locatedBy("/html/body/div[1]/div/form/div[1]/div[4]/div/div/select");

  public static final Target PET_ADD_BUTTON =
      Target.the("Pet add button").locatedBy("/html/body/div[1]/div/form/div[2]/div/button");

  // NEW PET VISIT
  public static final Target NEW_PET_VISIT_DATE_INPUT =
      Target.the("New visit date").locatedBy("/html/body/div[1]/div/form/div[1]/div[1]/div/input");

  public static final Target NEW_PET_VISIT_DESCRIPTION_INPUT =
      Target.the("New visit description")
          .locatedBy("/html/body/div[1]/div/form/div[1]/div[2]/div/input");

  public static final Target NEW_PET_VISIT_SUBMIT_BUTTON =
      Target.the("New visit submit").locatedBy("/html/body/div[1]/div/form/div[2]/div/button");
}
