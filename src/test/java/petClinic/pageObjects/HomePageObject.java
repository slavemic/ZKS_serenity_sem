package petClinic.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://slavev.stepanek.app/")
public class HomePageObject extends PageObject {

  public static final Target SELECT_WELCOME_HEADER =
      Target.the("Welcome header").locatedBy("/html/body/div/div/h2");
}
