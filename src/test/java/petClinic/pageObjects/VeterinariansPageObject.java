package petClinic.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://slavev.stepanek.app/vets")
public class VeterinariansPageObject extends PageObject {

  public static final Target VIEW_VETS_AS_XML =
      Target.the("View vets as xml button").locatedBy("/html/body/div/div/div[2]/div[1]/a");

  public static final Target VIEW_VETS_AS_JSON =
      Target.the("View vets as jsom button").locatedBy("/html/body/div/div/div[2]/div[2]/a");

  public static final Target GET_VETS_BY_SPECIALITY =
      Target.the("aa")
          .locatedBy(
              "//*[@id=\"vetsTable\"]/tbody/tr[./td[2]/text()[contains(normalize-space(), '{0}')]]/td[1]");

  public static final Target VETS_XML_OR_JSON =
          Target.the("get vets XML or JSON").locatedBy("/*");
}
