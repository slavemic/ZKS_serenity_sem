package petClinic.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static petClinic.pageObjects.FindOwnersPageObject.*;
import static petClinic.pageObjects.VeterinariansPageObject.*;

public class Navigate {

  public static Task findOwnerNamed(String name) {
    return Task.where(
        "{0} finds owner named: #name",
        WaitUntil.the(FIND_OWNER_LASTNAME_INPUT, isVisible())
            .then(
                Enter.theValue(name)
                    .into(FIND_OWNER_LASTNAME_INPUT)
                    .then(Click.on(FIND_OWNER_BUTTON))));
  }

  public static Task findAllOwners() {
    return Task.where(
        "{0} find all owners",
        WaitUntil.the(FIND_OWNER_LASTNAME_INPUT, isVisible())
            .then(
                Enter.theValue("")
                    .into(FIND_OWNER_LASTNAME_INPUT)
                    .then(Click.on(FIND_OWNER_BUTTON))));
  }

  public static Task toVetsXML() {
    return Task.where(
            "{0} to vets xml",
            WaitUntil.the(VIEW_VETS_AS_XML, isVisible())
                    .then(Click.on(VIEW_VETS_AS_XML)));
  }

  public static Task toVetsJSON() {
    return Task.where(
            "{0} to vets xml",
            WaitUntil.the(VIEW_VETS_AS_JSON, isVisible())
                    .then(Click.on(VIEW_VETS_AS_JSON)));
  }

}
