package petClinic.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.JavaScriptClick;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromTarget;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static petClinic.pageObjects.FindOwnersPageObject.*;

public class Manage {

  // -----||  OWNER
  public static Task editOwner() {
    return Task.where(
        "{0} click on edit owner",
        WaitUntil.the(EDIT_OWNER_BUTTON, isVisible()).then(Click.on(EDIT_OWNER_BUTTON)));
  }

  public static Task addOwner() {
    return Task.where(
        "{0} click on add owner",
        WaitUntil.the(ADD_OWNER_BUTTON, isVisible()).then(Click.on(ADD_OWNER_BUTTON)));
  }

  public static Task fillOwnerFirstName(String firstName) {
    return Task.where(
        "{0} fill owners firstName: #firstName",
        WaitUntil.the(OWNER_FIRSTNAME_INPUT, isVisible())
            .then(Enter.theValue(firstName).into(OWNER_FIRSTNAME_INPUT)));
  }

  public static Task fillOwnerLastName(String lastName) {
    return Task.where(
        "{0} fill owners lastName: #lastName",
        WaitUntil.the(OWNER_LASTNAME_INPUT, isVisible())
            .then(Enter.theValue(lastName).into(OWNER_LASTNAME_INPUT)));
  }

  public static Task fillOwnerAddress(String address) {
    return Task.where(
        "{0} fill owners address: #address",
        WaitUntil.the(OWNER_ADDRESS_INPUT, isVisible())
            .then(Enter.theValue(address).into(OWNER_ADDRESS_INPUT)));
  }

  public static Task fillOwnerCity(String city) {
    return Task.where(
        "{0} fill owners city: #city",
        WaitUntil.the(OWNER_CITY_INPUT, isVisible())
            .then(Enter.theValue(city).into(OWNER_CITY_INPUT)));
  }

  public static Task fillOwnerTelephone(String telephone) {
    return Task.where(
        "{0} fill owners telephone: #telephone",
        WaitUntil.the(OWNER_TELEPHONE_INPUT, isVisible())
            .then(Enter.theValue(telephone).into(OWNER_TELEPHONE_INPUT)));
  }

  public static Task confirmOwnerEdit() {
    return Task.where(
        "{0} confirms owner edit",
        WaitUntil.the(OWNER_ADD_BUTTON, isVisible()).then(Click.on(OWNER_ADD_BUTTON)));
  }

  // -----||  PET
  public static Task editPet() {
    return Task.where(
        "{0} click on edit pet",
        WaitUntil.the(GET_PET_EDIT, isVisible()).then(Click.on(GET_PET_EDIT)));
  }

  public static Task addPet() {
    return Task.where(
        "{0} click on add pet",
        WaitUntil.the(ADD_NEW_PET_BUTTON, isVisible()).then(Click.on(ADD_NEW_PET_BUTTON)));
  }

  public static Task addPetVisit() {
    return Task.where(
        "{0} click on add pet",
        WaitUntil.the(GET_PET_VISIT, isVisible()).then(Click.on(GET_PET_VISIT)));
  }

  public static Task fillPetsName(String name) {
    return Task.where(
        "{0} fill pets name: #name",
        WaitUntil.the(PET_NAME_INPUT, isVisible()).then(Enter.theValue(name).into(PET_NAME_INPUT)));
  }

  public static Task fillPetsBirth(String birth) {
    return Task.where(
        "{0} fill pets birth date: #birth",
        WaitUntil.the(PET_BIRT_DATE_INPUT, isVisible())
            .then(Enter.theValue(birth).into(PET_BIRT_DATE_INPUT)));
  }

  public static Task fillPetsType(String type) {
    return Task.where(
        "{0} fill pets type: #type",
        WaitUntil.the(PET_TYPE_SELECT_INPUT, isVisible())
            .then(new SelectByVisibleTextFromTarget(PET_TYPE_SELECT_INPUT, type)));
  }

  public static Task confirmPetEdit() {
    return Task.where(
        "{0} confirms owner edit",
        WaitUntil.the(PET_ADD_BUTTON, isVisible()).then(JavaScriptClick.on(PET_ADD_BUTTON)));
  }

  public static Task fillPetsVisitDate(String date) {
    return Task.where(
        "{0} fill pet visit date: #date",
        WaitUntil.the(NEW_PET_VISIT_DATE_INPUT, isVisible())
            .then(Enter.theValue(date).into(NEW_PET_VISIT_DATE_INPUT)));
  }

  public static Task fillPetsVisitDescription(String desc) {
    return Task.where(
        "{0} fill pet visit description: #desc",
        WaitUntil.the(NEW_PET_VISIT_DESCRIPTION_INPUT, isVisible())
            .then(Enter.theValue(desc).into(NEW_PET_VISIT_DESCRIPTION_INPUT)));
  }

  public static Task confirmPetVisit() {
    return Task.where(
        "{0} confirms pet visit",
        WaitUntil.the(NEW_PET_VISIT_SUBMIT_BUTTON, isVisible())
            .then(JavaScriptClick.on(NEW_PET_VISIT_SUBMIT_BUTTON)));
  }
}
