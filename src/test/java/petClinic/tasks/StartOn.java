package petClinic.tasks;

import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import petClinic.pageObjects.FindOwnersPageObject;
import petClinic.pageObjects.HomePageObject;
import petClinic.pageObjects.VeterinariansPageObject;

public class StartOn {

  public static Task findOwnersPage() {
    return Task.where(
        "{0} start on find owners page", Open.browserOn().the(new FindOwnersPageObject()));
  }

  public static Task veterinariansPage() {
    return Task.where(
        "{0} tart on veterinarians page", Open.browserOn().the(new VeterinariansPageObject()));
  }

  public static Task homePage() {
    return Task.where("{0} tart on veterinarians page", Open.browserOn().the(new HomePageObject()));
  }
}
