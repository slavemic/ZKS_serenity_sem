package petClinic;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import petClinic.questions.*;
import petClinic.tasks.Manage;
import petClinic.tasks.Navigate;
import petClinic.tasks.StartOn;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "src/test/resources/ACTS/EditPet-mixed.csv")
public class ParametrizedPetEditTests {
  private Actor james = Actor.named("James");

  String name;
  String birthdate;
  String type;
  Boolean isvalid;

  @Managed private WebDriver theBrowser;

  @Before
  public void jamesCanBrowseTheWeb() throws IOException {
    givenThat(james).can(BrowseTheWeb.with(theBrowser));
  }

  @Test
  public void edit_pet_from_csv() {
    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james).attemptsTo(Navigate.findOwnerNamed("Potatos"));

    when(james)
        .attemptsTo(
            Manage.editPet(),
            Manage.fillPetsName(name),
            Manage.fillPetsBirth(birthdate),
            Manage.fillPetsType(type),
            Manage.confirmPetEdit());

    if (isvalid) {
      then(james)
          .should(
              seeThat(PetNameQuestion.name(), Matchers.equalTo(name)),
              seeThat(PetBirthDateQuestion.date(), Matchers.equalTo(birthdate.replace('/', '-'))),
              seeThat(PetTypeQuestion.type(), Matchers.equalTo(type)));

    } else {
      if (name.length() > 30) {
        then(james)
            .should(
                seeThat(
                    CreationErrorQuestion.headerText(), Matchers.equalTo("Something happened...")));
      } else {
        then(james).should(seeThat(OwnerInputHelperQuestion.helperText(), Matchers.greaterThan(0)));
      }
    }
  }

  public void setName(String name) {
    if (name.equals("--")) {
      this.name = "";
      return;
    }
    this.name = name;
  }

  public void setBirthdate(String birthdate) {
    if (birthdate.equals("--")) {
      this.birthdate = "";
      return;
    }
    this.birthdate = birthdate;
  }

  public void setType(String type) {
    if (type.equals("elephant")) {
      this.type = "";
      return;
    }
    this.type = type;
  }

  public void setIsvalid(String valid) {
    this.isvalid = Boolean.parseBoolean(valid);
  }
}
