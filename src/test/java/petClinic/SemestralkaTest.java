package petClinic;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import petClinic.questions.*;
import petClinic.tasks.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static petClinic.questions.CurrentUserUrlQuestion.currentUrl;

@RunWith(SerenityRunner.class)
public class SemestralkaTest {

  Actor james = Actor.named("James");

  @Managed private WebDriver theBrowser;

  @Before
  public void before() throws IOException {
    givenThat(james).can(BrowseTheWeb.with(theBrowser));
  }

  @Test
  public void should_be_able_to_find_all_owners() {
    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james).attemptsTo(Navigate.findAllOwners());
    then(james)
        .should(
            seeThat(
                currentUrl(theBrowser.getCurrentUrl()),
                Matchers.equalTo("https://slavev.stepanek.app/owners?lastName=")));
    then(james)
        .should(seeThat(FoundOwnersQuestion.theDisplayedOwnersCount(), Matchers.greaterThan(0)));
  }

  @Test
  public void should_be_able_to_edit_owner() {
    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james).attemptsTo(Navigate.findOwnerNamed("Franklin"));

    then(james).should(seeThat(OwnersNameQuestion.firstName(), Matchers.equalTo("Dominik")));

    when(james)
        .attemptsTo(
            Manage.editOwner(), Manage.fillOwnerFirstName("Potato"), Manage.confirmOwnerEdit());
    then(james).should(seeThat(OwnersNameQuestion.firstName(), Matchers.equalTo("Potato")));

    when(james)
        .attemptsTo(
            Manage.editOwner(), Manage.fillOwnerFirstName("Dominik"), Manage.confirmOwnerEdit());
    then(james).should(seeThat(OwnersNameQuestion.firstName(), Matchers.equalTo("Dominik")));
  }

  @Test
  public void should_be_able_to_add_and_edit_pet() {
    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james).attemptsTo(Navigate.findOwnerNamed("Franklin"));
    then(james).should(seeThat(OwnersNameQuestion.firstName(), Matchers.equalTo("Dominik")));

    when(james)
        .attemptsTo(
            Manage.addPet(),
            Manage.fillPetsName("a"),
            Manage.fillPetsBirth("2005/08/08"),
            Manage.fillPetsType("cat"),
            Manage.confirmPetEdit());
    then(james).should(seeThat(PetNameQuestion.name(), Matchers.equalTo("a")));

    when(james).attemptsTo(Manage.editPet(), Manage.fillPetsName("aa"), Manage.confirmPetEdit());
    then(james).should(seeThat(PetNameQuestion.name(), Matchers.equalTo("aa")));
  }

  @Test
  public void should_be_able_to_add_pet_visit() {
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    DateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();

    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james).attemptsTo(Navigate.findOwnerNamed("Potatos"));
    when(james)
        .attemptsTo(
            Manage.addPetVisit(),
            Manage.fillPetsVisitDate(dateFormat.format(date)),
            Manage.fillPetsVisitDescription("Test description"),
            Manage.confirmPetVisit());
    then(james)
        .should(
            seeThat(PetVisitDateQuestion.date(), Matchers.equalTo(dateFormat2.format(date))),
            seeThat(PetVisitDescriptionQuestion.text(), Matchers.equalTo("Test description")));
  }

  @Test
  public void should_be_able_to_find_veterinary_dentist() {
    givenThat(james).wasAbleTo(StartOn.veterinariansPage());
    then(james)
        .should(
            seeThat(
                VeterinaryQuestion.getVetsBySpeciality("dentistry"),
                Matchers.contains("Linda Douglas")));
  }

  @Test
  public void should_be_able_to_parse_vets_xml() {
    givenThat(james).wasAbleTo(StartOn.veterinariansPage());
    when(james).attemptsTo(Navigate.toVetsXML());
    then(james)
            .should(
                    seeThat(
                            currentUrl(theBrowser.getCurrentUrl()),
                            Matchers.equalTo("https://slavev.stepanek.app/vets.xml")),
                    seeThat(VeterinaryXMLQuestion.doesContainXML(), Matchers.is(true)));
  }

  @Test
  public void should_be_able_to_parse_vets_json() {
    givenThat(james).wasAbleTo(StartOn.veterinariansPage());
    when(james).attemptsTo(Navigate.toVetsJSON());
    then(james)
            .should(
                    seeThat(
                            currentUrl(theBrowser.getCurrentUrl()),
                            Matchers.equalTo("https://slavev.stepanek.app/vets.json")),
                    seeThat(VeterinaryJSONQuestion.doesContainJSON(), Matchers.is(true)));
  }

  @After
  public void closeBrowser() {
    theBrowser.close();
  }
}
