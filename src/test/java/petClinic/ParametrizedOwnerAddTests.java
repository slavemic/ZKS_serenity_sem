package petClinic;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.annotations.Concurrent;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import petClinic.questions.CreationErrorQuestion;
import petClinic.questions.OwnerInputHelperQuestion;
import petClinic.questions.OwnersNameQuestion;
import petClinic.tasks.Manage;
import petClinic.tasks.StartOn;

import java.io.IOException;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "src/test/resources/ACTS/AddOwner-3.csv")
@Concurrent(threads = "4")
public class ParametrizedOwnerAddTests {
  private Actor james = Actor.named("James");

  String firstname;
  String lastname;
  String address;
  String city;
  String telephonenum;
  Boolean isvalid;

  @Managed private WebDriver theBrowser;

  @Before
  public void jamesCanBrowseTheWeb() throws IOException {
    givenThat(james).can(BrowseTheWeb.with(theBrowser));
  }

  @Test
  public void create_new_owner_from_csv() {
    givenThat(james).wasAbleTo(StartOn.findOwnersPage());
    when(james)
        .attemptsTo(
            Manage.addOwner(),
            Manage.fillOwnerFirstName(firstname),
            Manage.fillOwnerLastName(lastname),
            Manage.fillOwnerAddress(address),
            Manage.fillOwnerCity(city),
            Manage.fillOwnerTelephone(telephonenum),
            Manage.confirmOwnerEdit());

    if (isvalid) {
      then(james).should(seeThat(OwnersNameQuestion.firstName(), Matchers.equalTo(firstname)));
    } else {
      if (firstname.length() > 30
          || lastname.length() > 30
          || address.length() > 255
          || city.length() > 80) {
        then(james)
            .should(
                seeThat(
                    CreationErrorQuestion.headerText(), Matchers.equalTo("Something happened...")));
      } else {
        then(james).should(seeThat(OwnerInputHelperQuestion.helperText(), Matchers.greaterThan(0)));
      }
    }
  }

  public void setFirstname(String firstname) {
    if (firstname.equals("--")) {
      this.firstname = "";
      return;
    }
    this.firstname = firstname;
  }

  public void setLastname(String lastname) {
    if (lastname.equals("--")) {
      this.lastname = "";
      return;
    }
    this.lastname = lastname;
  }

  public void setAddress(String address) {
    if (address.equals("--")) {
      this.address = "";
      return;
    }
    this.address = address;
  }

  public void setCity(String city) {
    if (city.equals("--")) {
      this.city = "";
      return;
    }
    this.city = city;
  }

  public void setTelephonenum(String telephonenum) {
    if (telephonenum.equals("--")) {
      this.telephonenum = "";
      return;
    }
    this.telephonenum = telephonenum;
  }

  public void setIsvalid(String valid) {
    this.isvalid = Boolean.parseBoolean(valid);
  }
}
